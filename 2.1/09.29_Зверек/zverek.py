from threading import Timer
class Zver:
    def __init__(self, speed, hunger=0, happy=0, death=False):
        self.__speedhungry=speed
        self.__hunger=hunger
        self.__happy=happy
        self.__death=death
        self.__timer=Timer(0, print)
        self.__timer1=Timer(0, print)
    @property
    def dead(self):
        return self.__death
    def happiness(self):
        self.__timer1.cancel()
        self.__happy-=self.__speedhungry
        if self.__happy<-100:
            print('\nЗверь заскучал и ушел\n')
            self.stop()
        else:
            print(f'Радость: {self.__happy}', end='\n')
            self.__timer1=Timer(1, self.happiness)
            self.__timer1.start()
    def bring_happy(self):
        if self.__happy>0:
            print('\nЗверек наигрался\n')
            self.__happy=0
        self.__happy+=self.__speedhungry
    def eating(self):
        self.__timer.cancel()
        self.__hunger+=self.__speedhungry
        if self.__hunger>100:
            print('\nЗверь оголодал и ушел\n')
            self.stop()
        else:
            print(f'Голод: {self.__hunger}', end='\n')
            self.__timer=Timer(1, self.eating)
            self.__timer.start()
    def feeding(self):
        if self.__hunger<0:
            print('\nЗверек сыт\n')
            self.__hunger=0
        self.__hunger-=self.__speedhungry
    def stop(self):
        self.__death=True       
        self.__timer.cancel()
        self.__timer1.cancel()
z=Zver(10)
while not z.dead:
    cmd=input('Введите команду: s-старт, h-поиграть, f-кормить\n')
    match cmd:
        case 's':
            z.eating(),z.happiness()
        case 'h':
            z.bring_happy()
        case 'f':
            z.feeding()
        case 'x':
            z.stop()
            gameover=True
