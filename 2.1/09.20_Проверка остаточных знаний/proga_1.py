def task16():
    from random import randint
    board=[['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'],
        ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'],
        ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'],
        ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'],
        ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'],
        ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'],
        ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'],
        ['o', 'o', 'o', 'o', 'o', 'o', 'o', 'o']]
    for i in range(8):
        for j in range(8):
            if i==0 and j<6:
                board[i][j]='+'
            if j==5 and i>4:
                board[i][j]='+'
            if i==3 and j>4:
                board[i][j]='+'
            if i>1 and i<7 and j==2:
                board[i][j]='+'
            if i==2 and j==0:
                board[i][j]='+'
    for z in range(8):
        print(board[z])
def task15():
    n=int(input('Введите N: '))
    m=int(input('Введите M: '))
    for i in range(1, n+1):
        for j in range(1, m+1):
            print(i*j, end='\t')
        print()
def task14_2():
    n=float(input('Введите сумму в долларах, которую хотите конвертировать в рубли:\n'))
    print('Ваша сумма в рублях:', n*100)
def task14_1():
    n=float(input('Введите сумму в рублях, которую хотите конвертировать в доллары:\n'))
    print('Ваша сумма в долларах:', n/100)
def task13():
    def f(l,N):
        if N<=0:
            return 0
        else:
            return f(l,N-1) + l[N-1]
    l = [1, 2, 3, 4, 5, 6]
    N = len(l)
    print('Входные данные:', l)   
    print('Выходные данные:', f(l,N))
def task12():
    l=[1,2,3,4,5,6]
    print('Входные данные:', l)
    print('Выходные данные:', l[-1::-1])
def task11():
    l=[1,1,1,1,1,2]
    print('Входные данные:', l)
    print('Выходные данные:')
    print(l[len(l)-1])
    print(l[-1])
    print(l.pop()) 
def task10():
        d=4
        k=0
        n=1
        while k!=d:
            sum=0
            for i in range(1, n//2+1):
                if n%i==0:
                    sum+=i
            if n==sum:
                print(n)
                k+=1
            n+=1
def task9():
    n=int(input('Введите начало отрезка:\n'))
    m=int(input('Введите конец отрезка:\n'))
    for i in range(n, m+1):
        for j in str(i):
            if int(j)==0 or i%int(j)!=0:
                f=False
                break
            f=True
        if f:
            print(i)
def task8():
    n=int(input('Введите начало отрезка: \n'))
    m=int(input('Введите конец отрезка: \n'))
    f=False
    for a in range(n, m+1):
        for b in range(a+1, m+1):
            for c in range(n, m+1):
                if a**2+b**2==c**2:
                    print(a, b, c)
                    f=True
    if f==False:
        print('В отрезке нет троек Пифагора')
def task7():
    n=int(input("Введите число <250: \n"))
    k=0
    for i in range(1, n+1):
        for j in range(1, i+1):
            if i%j==0:
                k+=1
        print(i, k)
        k=0
def task6():
    n=int(input("Введите число: \n"))
    sum=0
    for i in range(1, n+1):
        sum+=i
    print("Summary:", sum)
    print("Even:", n//2)
    print("Odd:", n//2+1)
def task5_2():
    x=int(input("Введите число от 1 до 12: \n"))
    if x==12 or x==1 or x==2:
        print("Это зима")
    elif x==3 or x==4 or x==5:
        print("Это весна")
    elif x==6 or x==7 or x==8:
        print("Это лето")
    elif x==9 or x==10 or x==11:
        print("Это осень")
    else:
        print("Введите число от 1 до 12!")
def task5_1():
    x=int(input("введите число от 1 до 12: \n"))
    match x:
        case 12 | 1 | 2:
            print("Это зима")
        case 3 | 4 | 5:
            print("Это весна")
        case 6 | 7 | 8:
            print("Это лето")
        case 9 | 10 | 11:
            print("Это осень")
        case __:
            print("Введите число от 1 до 12!")
def task4():
    x=int(input("Введите число от 1 до 250: \n"))
    def f(n):
        if n==0:
            return 0
        if n==1:
            return 1
        return f(n-1)+f(n-2)
    T=False
    for n in range(15):
        x1=f(n)
        if x==x1:
            T=True
    if T==True:
        print("Это число Фибоначчи")
    else:
        print("Это не число Фибоначчи")
def task3_2():
    x=int(input("Введите число от 0 до 100 \n"))
    print(x*x*x*x*x)
def task3_1():
    x=int(input("Введите число от 0 до 100 \n"))
    print(x**5)
def task2_2():
    sum=0
    def f():
        print('Введите общее количество вводимых чисел')
        while True:
            try:
                return int(input())
            except ValueError:
                print('Введите целое число')
    n=f()
    for i in range(n):
        def f1():
            print('Введите целое число')
            while True:
                try:
                    return int(input())
                except ValueError:
                    print('Введите целое число')
        sum+=f1()
    print('', sum)
def task2_1():
    def f():
        print('Введите целое число')
        while True:
            try:
                return int(input())
            except ValueError:
                print("Введите целое число")
    a=f()
    def f1():
        print('Введите целое число')
        while True:
            try:
                return int(input())
            except ValueError:
                print("Введите целое число")
    b=f1()
    print(a+b)
def task1():
    a=1
    b=2
    c=3
    print("Start values: a = 1, b = 2, c = 3")
    sum=a+b+c
    b=c
    c=a
    a=sum-b-c
    print("Finish values: a =", a,"b =", b,"c =", c)
while True:
    inpt=input('Введите номер задачи, если задача делится на две части,\nто нужную часть дописывать нижним подчеркиванием, слева от основного номера,\nнапример: n_1, для выходите нажмите q:\n')
    match inpt:
        case '1':
            task1()
        case '2_1':
            task2_1()
        case '2_2':
            task2_2()
        case '3_1':
            task3_1()
        case '3_2':
            task3_2()
        case '4':
            task4()
        case '5_1':
            task5_1()
        case '5_2':
            task5_2()
        case '6':
            task6()
        case '7':
            task7()
        case '8':
            task8()
        case '9':
            task9()
        case '10':
            task10()
        case '11':
            task11()
        case '12':
            task12()
        case '13':
            task13()
        case '14_1':
            task14_1()
        case '14_2':
            task14_2()
        case '15':
            task15()
        case '16':
            task16()
        case 'q':
            print('Вы вышли из программы')
            break    

