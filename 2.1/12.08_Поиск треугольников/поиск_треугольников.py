from math import hypot
f = open('plist.txt', 'r')
lines=f.read()
coords = lines.replace('[', '').replace(']', ',').replace(' ', '').rstrip(',').split(',')

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def print_coords(self):
        print(self.x, self.y)

class Triangle:
    def __init__(self, p1, p2, p3):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
    
    def print_coords(self):
        print(f'Точка 1: {self.p1.x}, {self.p1.y}')
        print(f'Точка 2: {self.p2.x}, {self.p2.y}')
        print(f'Точка 3: {self.p3.x}, {self.p3.y}')

    def calculate_area(self):
        a=hypot(self.p2.x-self.p1.x, self.p2.y-self.p1.y)
        b=hypot(self.p3.x-self.p2.x, self.p3.y-self.p2.y)
        c=hypot(self.p1.x-self.p3.x, self.p1.y-self.p3.y)
        p=(a+b+c)/2
        s=(p*(p-a)*(p-b)*(p-c))
        if s>0:
            return s**0.5
        else:
            return 0
        
    def __eq__(self, other):
        return self.calculate_area() == other.calculate_area()

    def __ne__(self, other):
        return self.calculate_area() != other.calculate_area()

    def __lt__(self, other):
        return self.calculate_area() < other.calculate_area()

    def __le__(self, other):
        return self.calculate_area() <= other.calculate_area()

    def __gt__(self, other):
        return self.calculate_area() > other.calculate_area()

    def __ge__(self, other):
        return self.calculate_area() >= other.calculate_area()

points=[]
for i in range(0, len(coords), 2):
    x=coords[i]
    y=coords[i+1]
    obj=Point(int(x), int(y))
    points.append(obj)

triangles=[]
for i in range(0, len(points)):
    p1=points[i]
    for j in range(i+1, len(points)):
        p2=points[j]
        for k in range(j+1, len(points)):
            p3=points[k]
            triangle=Triangle(p1, p2, p3)
            triangles.append(triangle)

min_triangle = triangles[0]
for triangle in triangles:
    if triangle < min_triangle:
        min_triangle = triangle

max_triangle = triangles[0]
for triangle in triangles:
    if triangle > max_triangle:
        max_triangle = triangle

print(f'Треугольник с минимальной площадью: {min_triangle.calculate_area()}')
min_triangle.print_coords()
print(f'Треугольник с максимальной площадью: {max_triangle.calculate_area()}')
max_triangle.print_coords()

