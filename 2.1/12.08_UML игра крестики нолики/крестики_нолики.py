import pygame
pygame.init()
pygame.font.init()

font=pygame.font.SysFont('Arial', 50)  

WIDTH = 900
HEIGHT = 900
screen = pygame.display.set_mode((WIDTH, HEIGHT))

linewidth=15
cellsize=(WIDTH//3)

RED=(255, 0, 0)
BLACK=(0, 0, 0)
WHITE=(255, 255, 255)

x_positions=set()
o_positions=set()


cells=[]

current_player='X'

def switch_player():
    global current_player
    if current_player=='X':
        current_player='O'
    elif current_player=='O':
        current_player='X'

class Cell():
    def __init__(self, row, col, cellsize, default_image, x_image, o_image, cell_number):
        self.row=row
        self.col=col
        self.cellsize=cellsize
        self.image=pygame.image.load(default_image).convert_alpha()
        self.x_image=x_image
        self.o_image=o_image
        self.cell_number=cell_number
        self.rect=self.image.get_rect(topleft=(col * cellsize, row * cellsize))
        
    def switch_pic(self):
        global current_player
        if current_player=='X':
            self.image=pygame.image.load(self.x_image).convert_alpha()
            x_positions.add(self.cell_number)
            switch_player()
        elif current_player=='O':
            self.image=pygame.image.load(self.o_image).convert_alpha()
            o_positions.add(self.cell_number)
            switch_player()
            
    def draw(self):
        screen.blit(self.image, self.rect.topleft)

class Game():
    def __init__(self, x_positions, o_positions):
        self.x_positions=x_positions
        self.o_positions=o_positions


    def check_winner(self):
        winning_combinations = [(1, 2, 3), (4, 5, 6), (7, 8, 9),
                                (1, 4, 7), (2, 5, 8), (3, 6, 9),
                                (1, 5, 9), (3, 5, 7)]
        for combo in winning_combinations:
            if set(combo).issubset(x_positions):
                return 'X'
            if set(combo).issubset(o_positions):
                return 'O'
        return False

    def is_board_full(self):
        if (len(x_positions)==5 and len(o_positions)==4) or (len(x_positions)==4 and len(o_positions)==5):
            return True
        return False


cell_number=0
for row in range(3):
    for col in range(3):
        cell_number+=1
        curr_cell = Cell(row, col, cellsize, 'cell.png', 'x.png', 'o.png', cell_number)
        cells.append(curr_cell)

def gameLoop():
    running=True
    game=Game(x_positions, o_positions)
    while running:
        
        screen.fill(WHITE)
        for cell in cells:
            cell.draw()
        
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    running = False
            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_x, mouse_y = pygame.mouse.get_pos()
                for cell in cells:
                    if cell.rect.collidepoint(mouse_x, mouse_y):
                        cell.switch_pic()
            
        if game.check_winner()=='X':
            for cell in cells:
                del cell
            pygame.draw.line(screen, RED, (200, 20), (350, 50))
            screen.fill(WHITE)
            text=font.render('Игрок X победил! Нажмите Q для выхода', 1, BLACK)
            screen.blit(text, (50, 400))

        elif game.check_winner()=='O':
            for cell in cells:
                del cell
            screen.fill(WHITE)
            text=font.render('Игрок O победил! Нажмите Q для выхода', 1, BLACK)
            screen.blit(text, (50, 400))

        if game.is_board_full():
            for cell in cells:
                del cell
            screen.fill(WHITE)
            text=font.render('Ничья! Нажмите Q для выхода', 1, BLACK)
            screen.blit(text, (150, 400))


        pygame.display.flip()

    pygame.quit()
gameLoop()
