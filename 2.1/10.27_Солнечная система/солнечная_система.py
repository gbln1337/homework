import pygame
import math
pygame.init()

font=pygame.font.SysFont('Arial', 20)  

WIDTH = 1600
HEIGHT = 1600
screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()

curr_size=0
curr_sundistance=0
curr_time=0
curr_name=''
i=0

class Planet(pygame.sprite.Sprite):
    def __init__(self, image, radius, pos, sundistance, name, speed):
        super().__init__()
        self.image=pygame.image.load(image).convert_alpha()
        self.radius=radius
        self.image=pygame.transform.scale(self.image, (self.radius, self.radius))
        self.pos=pos
        self.sundistance=sundistance
        self.name=name
        self.speed=speed
        self.rect=self.image.get_rect()
        self.rect.center=self.pos
        self.angle=0

    def moving(self):
        screen.blit(self.image, self.rect)
        if self.angle<=360:
            angle = self.angle * 3.14 / 180
            self.rect.centerx = self.sundistance * math.sin(angle) + WIDTH/2
            self.rect.centery = self.sundistance * math.cos(angle) + HEIGHT/2
            self.angle+=self.speed
        else:
            self.angle=0
        
    def properties(self):
        return [self.radius, self.pos, self.sundistance, self.name]

    
class Sun(Planet):        
    def moving(self):
        screen.blit(self.image, self.rect)

    def properties(self):
        return [self.radius, self.pos, 0, self.name]
        
sun=Sun('pics/sun.png', 100, [WIDTH/2, HEIGHT/2], 0, 'Солнце', 0)
mercury=Planet('pics/mercury.png', 25, [WIDTH/2, HEIGHT/2], 100, 'Меркурий', 2.3)
venera=Planet('pics/venera.png', 50, [WIDTH/2, HEIGHT/2], 150, 'Венера', 2.2) 
earth=Planet('pics/earth.png', 50, [WIDTH/2, HEIGHT/2], 210, 'Земля', 2.1)
mars=Planet('pics/mars.png', 40, [WIDTH/2, HEIGHT/2], 270, 'Марс', 2)
jupiter=Planet('pics/jupiter.png', 75, [WIDTH/2, HEIGHT/2], 350, 'Юпитер', 1.6)
saturn=Planet('pics/saturn.png', 60, [WIDTH/2, HEIGHT/2], 420, 'Сатурн', 1.5)
uran=Planet('pics/uran.png', 40, [WIDTH/2, HEIGHT/2], 470, 'Уран', 1.2)
neptun=Planet('pics/neptun.png', 45, [WIDTH/2, HEIGHT/2], 520, 'Нептун', 1)

objects=[sun, mercury, venera, earth, mars, jupiter, saturn, uran, neptun]

def gameLoop():
    global curr_sundistance, curr_size, curr_time, curr_name
    time=0
    running = True
    n = 0
    while running:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    running = False
            if event.type==pygame.KEYDOWN:
                if event.key == pygame.K_n:
                    if n>=len(objects):
                        n=0
                    print(objects[n].properties())
                    curr_size=objects[n].properties()[0]
                    curr_sundistance=objects[n].properties()[2]
                    curr_name=objects[n].properties()[3]
                    n+=1
            if event.type == pygame.QUIT:
                running = False
        if time==60:
            screen.fill((0, 0, 0))

            curr_time+=1

            for planet in objects:
                planet.moving()

            
            text_mass=font.render(f"Для смены планеты нажмите N", True, (255, 255, 255))
            screen.blit(text_mass, (1300, 1100))
            text_mass=font.render(f"Название планеты: {curr_name}", True, (255, 255, 255))
            screen.blit(text_mass, (1350, 1140))
            text_mass=font.render(f"Диаметр: {curr_size}", True, (255, 255, 255))
            screen.blit(text_mass, (1350, 1160))
            text_pos=font.render(f"Удаленность от Солнца: {curr_sundistance}", True, (255, 255, 255))
            screen.blit(text_pos, (1350, 1180))
            text_time=font.render(f"Времени прошло(сек): {curr_time}", True, (255, 255, 255))
            screen.blit(text_time, (1350, 1200))

            time=0
        else:
            time+=1
        
        

        pygame.display.update()
    pygame.quit()
    quit()
 
gameLoop()
