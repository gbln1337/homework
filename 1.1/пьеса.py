roles={} #словарь с ролями
counter=0 #локальный счетчик
numroles=0 #количество ролей
roles['Слова автора']=[] #поиск слов автора
f=open("roles.txt", "r", encoding='utf-8')

#добавление ключей словаря:

for s in f.readlines(): 
    if counter==0:
        counter=1
        continue #пропуск первой строки "roles:"
    if s!='textLines:\n':
        roles[s[:len(s)-1]]=[] #добавление ключа
        counter=counter+1
    if s=='textLines:\n': #остановка цикла перед "textLines:"
        numroles=counter
        break
counter=1
currole=" " #переменная текущей роли
f1=open('roles.txt','r',encoding='utf-8')

#добавление значений к ключам:

for s in f1.readlines():
    if counter > numroles+2:
        r_rep = s.split(': ') #переменной r_rep задается значение переменной s после ": "

        #добавление к ключам словаря реплик 
        if r_rep[0] in roles.keys():
            replics=roles[r_rep[0]] #в переменную реплик записывается первое значение r_rep 
            currole = r_rep[0] #в текущую роль записывается значение реплики
        else:
            roles[currole].append(r_rep[0]) #добавление в ключ текущей роли значение реплики 
        if s.find("(") != -1: #добавление слов автора исходя из поиска скобок
            in_open=s.find('(')
            in_close=s.find(')')
            replics=s[in_open+1:in_close]
            roles['Слова автора'].append(replics)
    counter+=1
for i in roles:
    print(f"{i}: ") #печать "Слова автора:"
    for x in roles[i]:
        print(x) #печать словаря
    print()

