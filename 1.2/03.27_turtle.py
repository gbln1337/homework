import turtle
stylus=turtle.Turtle()
screen=turtle.Screen()
alpha=1
screen.tracer(0)
stylus.width(5)
while True:
    stylus.right(alpha)
    stylus.circle(120, 180)
    stylus.forward(200)
    stylus.circle(120, 180)
    stylus.forward(200)
    screen.update()
    stylus.clear()


