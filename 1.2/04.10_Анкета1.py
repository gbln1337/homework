from tkinter import *
from tkinter import ttk
root=Tk()
frm=ttk.Frame(root, padding=10)
frm.grid()
k=0
first=StringVar()
second=StringVar()
third=StringVar()
def inputing():
    with open('results.txt', 'a') as f:
        f.write('1.'+str(first.get())+'\n')
        f.write('2.'+str(second.get())+'\n')
        f.write('3.'+str(third.get())+'\n')
        f.write('\n')
ttk.Label(frm, text='1. Какой ваш любимый фильм?', padding=5).grid(column=0, row=0)
ttk.Entry(frm, textvariable=first).grid(column=0, row=1)
ttk.Label(frm, text='2. Какой актер в этом фильме вам запомнился больше всего?', padding=5).grid(column=0, row=2)
ttk.Entry(frm, textvariable=second).grid(column=0, row=3)
ttk.Label(frm, text='3. В каком году снят фильм и кто его режиссер?', padding=5).grid(column=0, row=4)
ttk.Entry(frm, textvariable=third).grid(column=0, row=5)
ttk.Button(frm, text='Отправить', command=inputing).grid(column=0, row=6)
root.mainloop()
