import pygame
from random import randint
from bullet import Bullet
STEP = 8
class Mario(pygame.sprite.Sprite):
    def __init__(self):
        self.left=[]
        self.right=[]
        for i in range(1, 6):
            self.left.append(pygame.image.load(f"animations/l{i}.png"))
            self.right.append(pygame.image.load(f"animations/r{i}.png"))
        self.stay = pygame.image.load("animations/0.png")
        self.window = pygame.display.get_surface()
        self.move = 0
        self.image = self.stay
        self.rect = self.image.get_rect()
        self.bullets = pygame.sprite.Group()


    def init_pos(self):
        self.pos_x = randint(self.rect.w/2, self.window.get_width()-self.rect.w/2)
        self.pos_y = randint(self.window.get_height(), self.window.get_height()*2)
        self.rect.center = (self.pos_x, self.pos_y)

    def bullets(self):
        return self.bullets

    def shoot(self):
        self.bullets.add(Bullet(pos=(self.rect.x, self.rect.bottom)))

    def update(self):
        self.bullets.update()
        keys=pygame.key.get_pressed()
        if keys[pygame.K_d] and not keys[pygame.K_a]:
            if self.rect[0] + self.rect[2] <= self.window.get_width():
                self.move = (self.move + 1)%len(self.right)
                self.image = self.right[self.move]
                self.rect.move_ip(STEP, 0)

        if keys[pygame.K_a] and not keys[pygame.K_d]:
            if self.rect[0] - STEP > 0:
                self.move = (self.move+1)%len(self.left)
                self.image = self.left[self.move]
                self.rect.move_ip(-STEP, 0)
 
        if not keys[pygame.K_a] and not keys[pygame.K_d]:
            if self.move != 0:
                self.move = 0
                self.image = self.stay
        if keys[pygame.K_SPACE]:
            self.shoot()
        self.window.blit(self.image, self.rect) 



        
