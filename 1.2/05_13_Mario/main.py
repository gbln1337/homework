import pygame
from pygame.locals import *
import random
from enemies import Enemy
from mario import Mario
pygame.init()
top_surf=pygame.display.set_mode((300, 300))
pygame.display.set_caption("Pygame test")
game_clock=pygame.time.Clock()

enemies = pygame.sprite.Group()
ENEMY_NUM = 20
for i in range(ENEMY_NUM):
    enemies.add(Enemy())


my_font = pygame.font.SysFont("Comic Sans MS", 30)
text = my_font.render("YOU DIED...", True, (255, 0, 0))

m1 = Mario()

running=True
is_paused = False

while running:

    top_surf.fill((255, 255, 255))
    top_surf.blit(m1.image, m1.rect)

    if not is_paused:
        m1.update()
        if len(enemies)<ENEMY_NUM:
            enemies.add(Enemy())
        enemies.update()
        if pygame.sprite.spritecollideany(m1, enemies):
            is_paused = True
            top_surf.blit(text, text.get_rect())
        pygame.sprite.groupcollide(m1.bullets, enemies, True, True)
        pygame.display.update()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        key_pressed = pygame.key.get_pressed()
        if key_pressed[pygame.K_ESCAPE]:
            running = False

       
    pygame.display.flip()
    game_clock.tick(30)
    