import pygame
from random import randint
SPEED_RANGE = (4, 5)
class Enemy(pygame.sprite.Sprite):
    def __init__(self, *groups):
        super().__init__(*groups)
        self.image = pygame.transform.scale(pygame.image.load("spider.png"), (40, 40))
        self.rect = self.image.get_rect()
        self.window = pygame.display.get_surface()
        self.init_pos()
        self.speed = randint(*SPEED_RANGE)

    def init_pos(self):
        self.pos_x = randint(self.rect.w/2, self.window.get_width()-self.rect.w/2)
        self.pos_y = randint(self.window.get_height(), self.window.get_height()*2)
        self.rect.center = (self.pos_x, self.pos_y)

    def update(self):
        self.rect.y -= self.speed
        if self.rect.y < 0:
            self.init_pos()
        self.window.blit(self.image, self.rect)