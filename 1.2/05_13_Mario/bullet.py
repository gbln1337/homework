import pygame
from random import randint
SPEED_RANGE = (4, 5)
class Bullet(pygame.sprite.Sprite):
    def __init__(self, *groups, pos):
        super().__init__(*groups)
        self.image = pygame.transform.rotate(pygame.transform.scale(pygame.image.load("bullet.png"), (30, 30)), -90)
        self.rect = self.image.get_rect()
        self.window = pygame.display.get_surface()
        self.rect.center = (pos[0], pos[1])
        self.speed = randint(*SPEED_RANGE)

    def update(self):
        self.rect.y += self.speed
        screen = pygame.display.get_surface()
        if self.rect.y > screen.get_height():
            self.kill()
        self.window.blit(self.image, self.rect)