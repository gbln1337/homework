import pygame
import sys
width=300
height=300
pygame.init()
screen=pygame.display.set_mode((width, height))
cordX=150
cordY=150
colors = [
(255, 255, 255),
(230, 50, 230),
(0, 0, 0),
(125, 125, 125),
(64, 128, 255),
(0, 200, 64),
(225, 225, 0)
]
col=0
running=True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running=False
        pygame.draw.rect(screen, colors[int(col)], (cordX, cordY, 10, 10))
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                if cordY<=0:
                    cordY=0
                else:
                    cordY-=10
                    #pygame.draw.rect(screen, (0, 0, 0), (cordX, cordY+10, 10, 10))
            if event.key == pygame.K_a:
                if cordX<=0:
                    cordX=0
                else:
                    cordX-=10
                    #pygame.draw.rect(screen, (0, 0, 0), (cordX+10, cordY, 10, 10))
            if event.key == pygame.K_s:
                if cordY==290:
                    cordY=290
                else:
                    cordY+=10
                    #pygame.draw.rect(screen, (0, 0, 0), (cordX, cordY-10, 10, 10))
            if event.key == pygame.K_d:
                if cordX==290:
                    cordX=290
                else:
                    cordX+=10
                    #pygame.draw.rect(screen, (0, 0, 0), (cordX-10, cordY, 10, 10))
            if event.key == pygame.K_c:
                col+=1
                if col==7:
                    col=0
        pygame.display.update()
