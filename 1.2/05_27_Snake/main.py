import pygame
import time
import random
import tkinter as tk
import tkinter.messagebox as mb
 
pygame.init()
 
screen_width = 500
screen_height = 500
 
screen = pygame.display.set_mode((screen_width, screen_height))
 
clock = pygame.time.Clock()

pygame.mixer.music.load("саундтрек.mp3")
 
snake_block = 10
snake_speed = 15

rules = pygame.font.SysFont("comicsansms", 15)

def show_warn():
        msg="Змея врезалась в стену! Нажмите Q для выхода или C для продолжения"
        mb.showwarning("Змея врезалась(", msg)

def self_eating():
        msg="Змея съела сама себя! Нажмите Q для выхода или C для продолжения"
        mb.showwarning("Змея погибла(", msg)
        
def snake_drawing(snake_block, snake_list):
    for x in snake_list:
        pygame.draw.rect(screen, (0, 255, 0), [x[0], x[1], snake_block, snake_block])
 
def gameLoop():
    running = True
    game_close = False
 
    x1 = screen_width/2
    y1 = screen_height/2
 
    x2 = 0
    y2 = 0
 
    snake_list = []
    snake_length = 1
 
    foodx = round(random.randrange(0, screen_width - snake_block) / 10.0) * 10.0
    foody = round(random.randrange(0, screen_height - snake_block) / 10.0) * 10.0
    
    pygame.mixer.music.play(loops = -1, start = 0.0, fade_ms = 100)
    pygame.mixer.music.set_volume(0.1)
    
    while running:
        while game_close == True:
            screen.fill((255, 255, 255))
            pygame.display.update()
 
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        running = False
                        game_close = False
                        pygame.mixer.music.pause()
                    if event.key == pygame.K_c:
                        gameLoop()
                        pygame.mixer.music.play(loops = -1, start = 0.0, fade_ms = 1)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    x2 = -snake_block
                    y2 = 0
                elif event.key == pygame.K_d:
                    x2 = snake_block
                    y2 = 0
                elif event.key == pygame.K_w:
                    y2 = -snake_block
                    x2 = 0
                elif event.key == pygame.K_s:
                    y2 = snake_block
                    x2 = 0
 
        if x1 >= screen_width or x1 < 0 or y1 >= screen_height or y1 < 0:
            pygame.mixer.music.load("ударвстену.mp3")
            pygame.mixer.music.play(1)
            show_warn()
            game_close = True
            pygame.mixer.music.load("саундтрек.mp3")
            pygame.mixer.music.play(loops = -1, start = 0.0, fade_ms = 100)
            pygame.mixer.music.set_volume(0.1)
            
        x1 += x2
        y1 += y2
        screen.fill((255, 255, 255))
        
        text_value=rules.render("W-вверх, A-влево, S-вниз, D-вправо", True, (0, 0, 255))
        screen.blit(text_value, [0, 0])
        pygame.draw.rect(screen, (255, 0, 0), [foodx, foody, snake_block, snake_block])
        snake_head = []
        snake_head.append(x1)
        snake_head.append(y1)
        snake_list.append(snake_head)
        if len(snake_list) > snake_length:
            del snake_list[0]
 
        for x in snake_list[:-1]:
            if x == snake_head:
               self_eating()
               game_close = True
 
        snake_drawing(snake_block, snake_list)
 
 
        pygame.display.update()
 
        if x1 == foodx and y1 == foody:
            foodx = round(random.randrange(0, screen_width - snake_block) / 10.0) * 10.0
            foody = round(random.randrange(0, screen_height - snake_block) / 10.0) * 10.0
            snake_length += 1
 
        clock.tick(15)
 
    pygame.quit()
    quit()
 
 
gameLoop() 